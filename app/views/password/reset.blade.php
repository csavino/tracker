@extends('/layouts/default')
@section('content')

<div class="row">
    <div class="col-md-4">
<h3>Reset Password?</h3>

{{Form::open()}}
        <input type="hidden" name="token" value="{{$token}}">

<p>
    {{Form::label('email','Email:') }}
    {{Form::email('email',null,['class'=>'form-control'])}}
    {{$errors->first('email','<div class="error">:message</div>')}}
</p>


<p>
    {{Form::label('password','Password:') }}
    {{Form::password('password',['class'=>'form-control'])}}
</p>

<p>
    {{Form::label('password_confirmation','Password Confirmation:') }}
    {{Form::password('password_confirmation',['class'=>'form-control'])}}
</p>

{{Form::submit('submit')}}

{{Form::close()}}

        @if(Session::has('error'))
            <p class="error">{{Session::get('error') }}</p>
        @elseif(Session::has('status'))
            <p>{{Session::get('status') }} </p>
        @endif
    </div>
</div>


@stop