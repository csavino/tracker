@extends('/layouts/default')
@section('content')

<div class="row">
    <div class="col-md-4">
<h3>Need to reset your password?</h3>

{{Form::open()}}

<p>
    {{Form::label('email','Email:') }}
    {{Form::email('email',null,['class'=>'form-control'])}}
</p>

{{Form::submit('reset password')}}

{{Form::close()}}

        @if(Session::has('error'))
            <p class="error">{{Session::get('error') }}</p>
        @elseif(Session::has('status'))
            <p>{{Session::get('status') }} </p>
        @endif
    </div>
</div>


@stop