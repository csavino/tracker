@extends('/layouts/default')
@section('content')

<?php
$fragment =  explode('/',$_SERVER['REQUEST_URI']);
$focus = $fragment[1];
?>

<div class="row">
    <div class="col-md-12">
<h3>Show <?php echo ucwords($focus); ?></h3>
        <hr>
</div>
    </div>
<div class="row">
    <div class="col-md-8">
        @if(Session::has('message'))
        <div class="alert alert-success">{{Session::get('message')}}</div>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <style>
        .col-md-8   div   {
        border-top:1px solid silver;
        }
        </style>
    <p>
        <div><strong>Firstname:</strong></div>
        {{$user->firstname}}
    </p>
    <p>
        <div><strong>Lastname:</strong></div>
        {{$user->lastname}}
    </p>
    <p>
        <div><strong>Username:</strong></div>
        {{$user->username}}
    </p>
    <p>
        <div><strong>Email:</strong></div>
        {{$user->email}}
    </p>
    <p>
        <div><strong>Updated at:</strong></div>
        {{$user->updated_at}}
    </p>

    <p style="border-top:1px solid silver;padding-top:5px">
        <a class="btn btn-small btn-primary" href="{{URL::to('users')}}">Back</a>
    </p>

    </div>

    <div class="col-md-2">

    @include('layouts/sidebar')
    </div> <!-- end col-md-2 -->
    </div"><!-- row -->


@stop