@extends('/layouts/default')
@section('content')
<script type="text/javascript">
    $(document).ready(function() {
        $('#usergrid').dataTable(
            {
                "oLanguage": {
                    "sSearch": ""
                }
            }
        );
        $('.dataTables_filter input').attr("placeholder","Search");
    } );
</script>
<?php
$fragment =  explode('/',$_SERVER['REQUEST_URI']);
$focus = $fragment[1];
?>
    <div class="container" align="center>">

        <div class="row">
            <div class="col-md-12">
                <h3>All <?php echo ucwords($focus); ?></h3>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10">
                @if(Session::has('message'))
                <div class="alert alert-success">{{Session::get('message')}}</div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">

        <table class="table table-striped table-bordered" id="usergrid">
            <thead>
            <tr>
                <th>Id</th>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>Email</th>
                <th>Last Updated</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->firstname}}</td>
                <td>{{$user->lastname}}</td>
                <td>{{$user->email}}</td>
                <td>{{date_format($user->updated_at,'m/d/Y h:i:s a')}}</td>
                <td><a class="btn btn-xs btn-success" href="{{URL::to('users/'.$user->id)}}">Show</a></td>
                <td><a class="btn btn-xs btn-success" href="{{URL::to('users/'.$user->id.'/edit')}}">Edit</a></td>
                <td>
                {{Form::open(array('route'=>['users.destroy',$user->id],'method'=>'delete'))}}
                <button type="submit" class="btn btn-xs btn-warning">Delete</button>
                {{Form::close()}}
                </td>
            </tr>
            @endforeach
        </tbody>
        </table>
        <table>
            <tr>
                {{$users->links()}}
            </tr>
        </table>
                </div> <!-- end col-md-8" -->
            <div class="col-md-2">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><?php echo ucwords($focus) ?></h4>

                    </div>
                    <div id="sidebar" class="list-group">
                        <a href="/<?php echo $focus ?>/create" class="list-group-item active">
                            <i class="icon-dashboard"></i> Create
                        </a>
                         <a href="/<?php echo $focus ?>" class="list-group-item ">
                            <i class="icon-dashboard"></i> View All
                        </a>
                    </div> <!-- end sidebar -->
                </div><!-- end panel -->
            </div> <!-- end col-md-2 -->
        </div> <!-- end row -->
    </div> <!-- end container -->

@stop