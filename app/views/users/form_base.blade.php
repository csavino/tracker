    <p>
    {{Form::label('firstname','Firstname:') }}
    {{Form::text('firstname',null,['class'=>'form-control'])}}
    {{$errors->first('firstname','<div class="error">:message</div>')}}
    </p>
    <p>
       {{Form::label('lastname','Lastname:') }}
       {{Form::text('lastname',null,['class'=>'form-control'])}}
       {{$errors->first('lastname','<div class="error">:message</div>')}}
    </p>
    <p>
       {{Form::label('username','Username:') }}
       {{Form::text('username',null,['class'=>'form-control'])}}
       {{$errors->first('username','<div class="error">:message</div>')}}
    </p>

    <?php $fragment =  substr($_SERVER['REQUEST_URI'],1); if(strpos($fragment,'create')) { ?>

    <p>
        {{Form::label('password','Password:') }}
        {{Form::password('password',['class'=>'form-control'])}}
        {{$errors->first('password','<div class="error">:message</div>')}}
    </p>

    <?php } ?>
    <p>
    {{Form::label('email','Email:') }}
    {{Form::text('email',null,['class'=>'form-control'])}}
    {{$errors->first('email','<div class="error">:message</div>')}}
    </p>



