@extends('/layouts/default')
@section('content')

<?php
$fragment =  explode('/',$_SERVER['REQUEST_URI']);
$focus = $fragment[1];
?>



<div class="row">
    <div class="col-md-12">
<h3>Edit <?php echo ucwords($focus); ?></h3>
        <hr>
</div>
    </div>
<div class="row">
    <div class="col-md-8">
    {{Form::model($user,array('route'=>array('users.update',$user->id),'method'=>'PUT')) }}

   @include('users/form_base')

    <p>
    {{Form::submit('submit',array('class'=>'btn btn-primary'))}}

    </p>

    {{Form::close()}}
    </div>

    <div class="col-md-2">
        @include('layouts/sidebar')
    </div> <!-- end col-md-2 -->
    </div"><!-- row -->

@stop