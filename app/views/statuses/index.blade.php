@extends('/layouts/default')
@section('content')
<script type="text/javascript">
    $(document).ready(function() {
        $('#statusgrid').dataTable(
            {
                "oLanguage": {
                    "sSearch": ""
                }
            }
        );
        $('.dataTables_filter input').attr("placeholder","Search");
    } );
</script>
<?php
$fragment =  explode('/',$_SERVER['REQUEST_URI']);
$focus = $fragment[1];
?>
    <div class="container" align="center>">

        <div class="row">
            <div class="col-md-12">
                <h3>All <?php echo ucwords($focus); ?></h3>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                @if(Session::has('message'))
                <div class="alert alert-success">{{Session::get('message')}}</div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">

            <table id="statusgrid" class="table table-striped table-bordered" style="width:80%">
                <thead>
                <tr>
                    <th>Status</th>
                    <th>Last Updated</th>
                    <th>&nbsp;</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                @foreach($statuses as $status)
                <tr>
                    <td>{{$status->status}}</td>
                    <td>{{date_format($status->updated_at,'m/d/Y h:i:s a')}}</td>
                    <td><a class="btn btn-xs btn-success" href="{{URL::to('statuses/'.$status->status.'/edit')}}">Edit</a></td>
                    <td>
                    {{Form::open(array('route'=>['statuses.destroy',$status->status],'method'=>'delete'))}}
                    <button type="submit" class="btn btn-xs btn-warning">Delete</button>
                    {{Form::close()}}
                    </td>
                </tr>
                @endforeach
            </tbody>
            </table>
            <table>
                <tr>
                    {{$statuses->links()}}
                </tr>
            </table>
            </div> <!-- end col-md-8" -->
            <div class="col-md-2">
                @include('layouts/sidebar')
            </div> <!-- end col-md-2 -->
        </div> <!-- end row -->
    </div> <!-- end container -->

@stop