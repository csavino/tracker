@extends('/layouts/default')
@section('content')

<?php
$fragment =  explode('/',$_SERVER['REQUEST_URI']);
$focus = $fragment[1];
?>

<div class="row">
    <div class="col-md-12">
<h3>Edit <?php echo ucwords($focus); ?></h3>
        <hr>
</div>
    </div>
<div class="row">
    <div class="col-md-8">
    {{Form::model($status,array('route'=>array('statuses.update',$status->status),'method'=>'PUT')) }}

        <p>
            {{Form::label('status','Status:') }}
            {{Form::text('status',null,['class'=>'form-control'])}}
            {{$errors->first('status','<div class="error">:message</div>')}}
        </p>

    <p>
    {{Form::submit('submit',array('class'=>'btn btn-primary'))}}

    </p>

    {{Form::close()}}
    </div>

    <div class="col-md-2">
        @include('layouts/sidebar')
    </div><!-- end panel -->
    </div> <!-- end col-md-2 -->
</div"><!-- row -->

@stop