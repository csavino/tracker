@extends('/layouts/default')
@section('content')

<div class="jumbotron" style="position:relative;top:10px">
<h1> Welcome to Tracker</h1>
<p>An application built with PHP, MySql, Bootstrap & Laravel.</p>
</div>

<p>There are places to store and track information on Issues & Users:
<ol>
    <li>Anyone can view and add issues</li>
    <li>Logged in users can view, add, change and delete issues, users and keywords </li>
</ol>

@stop