@extends('/layouts/default')
@section('content')

<?php
$fragment =  explode('/',$_SERVER['REQUEST_URI']);
$focus = $fragment[1];
?>


<div class="row">
    <div class="col-md-12">
<h3>Show <?php echo ucwords($focus); ?></h3>
        <hr>
</div>
    </div>
<div class="row">
    <div class="col-md-8">
        @if(Session::has('message'))
        <div class="alert alert-success">{{Session::get('message')}}</div>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-md-8">
        <style>
         .col-md-8   div   {
                border-top:1px solid silver;
            }
        </style>
    <p>
        <div><strong>Title:</strong></div>
        {{$issue->name}}
    </p>
    <p>
        <div><strong>Status:</strong></div>
        {{$issue->status}}
    </p>
    <p>
        <div><strong>Assigned To:</strong></div>
        {{User::getFullname($issue->assigned_to)}}
    </p>
    <p>
        <div><strong>OS:</strong></div>
        {{$issue->os}}
    </p>
        <p>
        <div><strong>URL:</strong></div>
        {{$issue->url}}
        </p>
        <p>
        <div><strong>Authenticated:</strong></div>
        <?php if ($issue->authenticated){
            echo 'Yes';
        } else {
            echo 'No';
        } ?>
        </p>
        <p>
        <div><strong>Error Msg:</strong></div>
        {{$issue->error_msg}}
        </p>
        <p>
        <div><strong>Expected Output:</strong></div>
        {{$issue->expected_output}}
        </p>
        <p>
        <div><strong>Notes:</strong></div>
        {{$issue->notes}}
        <p>
        <div><strong>Updated By:</strong></div>
        {{User::getFullname($issue->updated_by)}}
        </p>
        <p>
        <div><strong>Last Updated:</strong></div>
        {{date_format($issue->updated_at,'m/d/Y h:i:s a')}}
        </p>

    <p style="border-top:1px solid silver;padding-top:5px">
        <a class="btn btn-small btn-primary" href="{{URL::to('issues')}}">Back</a>
    </p>

    </div>

    <div class="col-md-2">
        @include('layouts/sidebar')
    </div> <!-- end col-md-2 -->
</div"><!-- row -->


@stop