@extends('/layouts/default')
@section('content')
<?php
$fragment =  explode('/',$_SERVER['REQUEST_URI']);
$focus = $fragment[1];
?>

<div class="row">
    <div class="col-md-12">
<h3>New <?php echo ucwords($focus); ?></h3>
        <hr>
</div>
    </div>
<div class="row">
    <div class="col-md-8">

{{Form::open(['route'=>'issues.store','class'=>'trform']) }}

        <p>
            {{Form::label('name','Title:') }}
            {{Form::text('name',null,['class'=>'form-control'])}}
            {{$errors->first('name','<div class="error">:message</div>')}}
        </p>
        <p>
            {{Form::label('status','Status:')}}
            {{Form::select('status', array('default'=> '- Select -')+$status_options , null,['class'=>'form-control'])}}
            {{$errors->first('status','<div class="error">:message</div>')}}
        </p>
        <p>
            {{Form::label('assigned_to','Assigned To:')}}
            {{Form::select('assigned_to', array('default'=> '- Select -')+$user_options , null,['class'=>'form-control'])}}
            {{$errors->first('assigned_to','<div class="error">:message</div>')}}
        </p>
        @include('issues/form_base')
{{Form::close() }}
    </div>

    <div class="col-md-2">
        @include('layouts/sidebar')
    </div> <!-- end col-md-2 -->
</div"><!-- row -->


@stop