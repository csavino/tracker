@extends('/layouts/default')
@section('content')
<script type="text/javascript">
    $(document).ready(function() {
        $('#issuegrid').dataTable(
            {
                "oLanguage": {
                        "sSearch": ""
                    }
            }
        );
        $('.dataTables_filter input').attr("placeholder","Search");
    } );

</script>
<?php
$fragment =  explode('/',$_SERVER['REQUEST_URI']);
$focus = explode("?",$fragment[1])[0];
?>

    <div class="container" align="center>">

        <div class="row">
            <div class="col-md-12">
                <h3>All <?php echo ucwords($focus); ?></h3>
                <hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-10">
                @if(Session::has('message'))
                <div class="alert alert-success">{{Session::get('message')}}</div>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">

        <table class="table table-striped table-bordered" id="issuegrid">
            <thead>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Status</th>
                <th>Assigned To</th>
                <th>OS</th>
                <th>Last Updated</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @if(($issues))
                @foreach($issues as $issue)
                <tr>
                    <td>{{$issue->id}}</td>
                    <td>{{$issue->name}}</td>
                    <td>{{$issue->status}}</td>
                    <td>{{$issue->assigned_name}}</td>
                    <td>{{$issue->os}}</td>
                    <td>{{date_format($issue->updated_at,'m/d/Y h:i:s a')}}</td>
                    <td><a class="btn btn-xs btn-success" href="{{URL::to('issues/'.$issue->id)}}">Show</a></td>
                    <td><a class="btn btn-xs btn-success" href="{{URL::to('issues/'.$issue->id.'/edit')}}">Edit</a></td>
                    <td>
                    {{Form::open(array('route'=>['issues.destroy',$issue->id],'method'=>'delete'))}}
                    <button type="submit" class="btn btn-xs btn-warning">Delete</button>
                    {{Form::close()}}
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
        <table>
            <tr>
                @if($issues)
                {{$issues->links()}}
                @endif
            </tr>
        </table>
                </div> <!-- end col-md-10" -->
            <div class="col-md-2">
                @include('layouts/sidebar')
            </div> <!-- end col-md-2 -->
        </div> <!-- end row -->
    </div> <!-- end container -->

@stop