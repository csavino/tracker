


<p>
    {{Form::label('os','OS:')}}
    {{Form::text('os',null,['class'=>'form-control'])}}
    {{$errors->first('os','<div class="error">:message</div>')}}
</p>
<p>
    {{Form::label('url','URL:')}}
    {{Form::text('url',null,['class'=>'form-control'])}}
    {{$errors->first('url','<div class="error">:message</div>')}}
</p>
<p>
    {{Form::label('authenticated','Authenticated:')}}
    {{Form::radio('authenticated',1,['class'=>'form-control'])}}Yes
    &nbsp;
    {{Form::radio('authenticated',0,['class'=>'form-control'])}}No
    {{$errors->first('authenticated','<div class="error">:message</div>')}}
</p>
<p>
    {{Form::label('error_msg','Error Message:')}}
    {{Form::text('error_msg',null,['class'=>'form-control'])}}
    {{$errors->first('error_msg','<div class="error">:message</div>')}}
</p>
<p>
    {{Form::label('expected_output','Expected Output:')}}
    {{Form::textarea('expected_output',null,['class'=>'form-control'])}}
    {{$errors->first('expected_output','<div class="error">:message</div>')}}
</p>
<p>
    {{Form::label('notes','Notes:')}}
    {{Form::textarea('notes',null,['class'=>'form-control'])}}
    {{$errors->first('notes','<div class="error">:message</div>')}}
</p>

<p>
    {{Form::submit('submit',array('class'=>'btn btn-primary'))}}
</p>
