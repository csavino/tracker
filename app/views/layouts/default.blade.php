<html>
<head>
    <title>
        @section('title')
        Tracker
        @show
    </title>

    <!-- Optional Bootstrap theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap-theme.min.css">
    <!-- Data tables Bootstrap CDN -->
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/505bef35b56/integration/bootstrap/3/dataTables.bootstrap.css">
    <!-- Latest compiled and minified Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
    <!-- Data tables CDN -->
    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/plug-ins/505bef35b56/integration/bootstrap/3/dataTables.bootstrap.css">
    <!-- Site Custom CSS -->
    <link rel="stylesheet" href="/css/styles.css">

    <!-- JQuery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Latest compiled and minified Bootstrap JavaScript -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
    <!-- Data tables Javascript CDN -->
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10-dev/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="//cdn.datatables.net/plug-ins/505bef35b56/integration/bootstrap/3/dataTables.bootstrap.js"></script>

</head>

<body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Tracker</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/">Home</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Issues <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ URL::route('issues.create')}}">New Issue</a></li>
                        <li><a href="{{ URL::route('issues.index')}}">All Issues</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Users <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ URL::route('users.create')}}">New User</a></li>
                        <li><a href="{{ URL::route('users.index')}}">All Users</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Keywords <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ URL::route('statuses.create')}}">New Status</a></li>
                        <li><a href="{{ URL::route('statuses.index')}}">All Statuses</a></li>

                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav pull-right">
                <?php if(Auth::guest()) { ?>
                <li><a href="/login">Login</a></li>
                <?php } ?>
                <?php if(Auth::check()) { ?>
                <li><a href="/logout">Logout (<?php echo Auth::user()->username ?>) </a></li>
                <?php } ?>
           </ul>
        </div><!--/.nav-collapse -->

    </div>
</div>

<?php
$fragment =  explode('/',$_SERVER['REQUEST_URI']);
$focus = rtrim($fragment[1],'?q=');
?>

<div class="container"style="position:relative;top:50px">

    <!-- Main component for a primary marketing message or call to action -->

    @yield('content')


</div><!-- /.container -->


<!-- Bootstrap core JavaScript
   ================================================== -->




</body>
</html>