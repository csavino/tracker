<?php
$fragment =  explode('/',$_SERVER['REQUEST_URI']);
$focus = explode("?",$fragment[1])[0];
?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h4><?php echo ucwords($focus) ?></h4>
   </div>
    <div id="sidebar" class="list-group">
        <a href="{{ URL::route($focus.'.create')}}" class="list-group-item active">
            <i class="icon-dashboard"></i> Create
        </a>
        <a href="{{ URL::route($focus.'.index')}}" class="list-group-item ">
            <i class="icon-dashboard"></i> View All
        </a>
    </div> <!-- end sidebar -->
</div><!-- end panel -->