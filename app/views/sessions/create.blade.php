@extends('/layouts/default')
@section('content')

<div class="row">
<div class="col-md-12">
    <h3>Please Login</h3>
    <hr>
</div>
</div>
<div class="row">
<div class="col-md-6">
    {{Form::open(['route'=>'sessions.store'])}}

<p>
    {{Form::label('username','Username:') }}
    {{Form::text('username',null,['class'=>'form-control'])}}
    {{$errors->first('username','<div class="error">:message</div>')}}
</p>
<p>
    {{Form::label('password','Password:') }}
    {{Form::password('password',['class'=>'form-control'])}}
    {{$errors->first('password','<div class="error">:message</div>')}}
</p>
    <p>
        {{Form::submit('login',array('class'=>'btn btn-primary'))}}
    </p>
    &nbsp;
    <p>
        <a href="{{ URL::to('password/remind')}}" class="">
            Forgot Password?</a>
    </p>
    {{ Form::close() }}

@stop

</div>
</div>