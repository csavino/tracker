<?php namespace Repositories;
/**
 * Created by PhpStorm.
 * User: winpress
 * Date: 4/10/14
 * Time: 11:15 PM
 */

    use Illuminate\Support\ServiceProvider;

    class BackendServiceProvider extends ServiceProvider {

            public function register() {

                $this->app->bind('Repositories\ItemRepositoryInterface','Repositories\DbIssueRepository');
                $this->app->bind('Repositories\UserRepositoryInterface','Repositories\DbUserRepository');
                $this->app->bind('Repositories\StatusRepositoryInterface','Repositories\DbStatusRepository');
            }

    }