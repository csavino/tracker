<?php namespace Repositories;
/**
 * Created by PhpStorm.
 * User: winpress
 * Date: 4/10/14
 * Time: 10:51 PM
 */

use User;

class DBUserRepository implements UserRepositoryInterface {

    public function getAll(){

    }


    public function find($id){
        return User::findOrFail($id);
    }

    public function createRoute(){

        return 'users.create';
    }

    public function storeRoute(){
        return 'users.store';
    }

    public function showRoute(){
        return 'users.show';
    }

    public function indexRoute(){
        return 'index.store';
    }

    public function isValid($data){

        $user = new User();
        return $user->isValid($data);

    }




}