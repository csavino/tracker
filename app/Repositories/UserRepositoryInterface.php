<?php namespace Repositories;
/**
 * Created by PhpStorm.
 * User: winpress
 * Date: 4/10/14
 * Time: 10:59 PM
 */

interface UserRepositoryInterface{

    public function getAll();

    public function find($id);

    public function createRoute();

    public function storeRoute();

    public function showRoute();

    public function indexRoute();

    public function isValid($data);

}