<?php namespace Repositories;
/**
 * Created by PhpStorm.
 * User: winpress
 * Date: 4/10/14
 * Time: 10:59 PM
 */

interface ItemRepositoryInterface{

    public function getAll();

    public function indexAll();

    public function indexQuery($query);

    public function find($id);

    public function createRoute();

    public function indexRoute();

    public function editRoute();

    public function showRoute();

    public function isValid($data);

    public function add($data);

    public function modify($data,$key);
}