<?php namespace Repositories;
/**
 * Created by PhpStorm.
 * User: winpress
 * Date: 4/10/14
 * Time: 10:51 PM
 */

use Status;

class DBStatusRepository implements StatusRepositoryInterface {

    public function getAll(){
       // return Issue::All();
        return "This is a DbStatusRepository message from getAll";
    }


    public function find($id){
        return Status::findOrFail($id);
    }

    public function createRoute(){
        return 'statuses.create';
    }

    public function storeRoute(){
        return 'statuses.store';
    }

    public function indexRoute(){
        return 'statuses.index';
    }

    public function isValid($data){

        $status = new Status();
        return $status->isValid($data);

    }

    public function statusOptions() {

        // queries the db table, orders by status and lists status
        $client_optons = DB::table('clients')->orderBy('client_name', 'asc')->lists('client_name','id');

        return View::make('projects.create', array('client_options' => $client_options));
    }
}