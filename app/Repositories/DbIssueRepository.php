<?php namespace Repositories;
/**
 * Created by PhpStorm.
 * User: winpress
 * Date: 4/10/14
 * Time: 10:51 PM
 */

use Issue;
use Auth;
use User;

    class DBIssueRepository implements ItemRepositoryInterface {

        public $focus = 'issues';
        public $focusItem = 'issue';

        public function getAll(){
            return "This is a DBIssueRepository message from getAll";
        }

        public function indexAll(){
            $issues= Issue::orderBy('name','ASC')->paginate(10);

            foreach($issues as $issue){
                $issue=$this->getFullnamesById($issue);
            }
            return $issues;
        }

        public function indexQuery($query){

            $user = User::where('fullname','LIKE',"%$query%")->first();

            if($user){
                $issues = Issue::where('assigned_to','=',$user->id)->paginate(10);
                foreach($issues as $issue){
                    $issue=$this->getFullnamesById($issue);
                }
            }
            if (!empty($issues)){
            return $issues;
            }else {
                return;
            }

        }


        public function find($id){
            return Issue::findOrFail($id);
        }

        public function createRoute(){
            return 'issues.create';
        }

        public function indexRoute(){
            return 'issues.index';
        }

        public function editRoute(){
            return 'issues.edit';
        }

        public function showRoute(){
            return 'issues.show';
        }

        public function isValid($data){
            $issue = new Issue();
            return $issue->isValid($data);

        }

        public function add($data){
            $issue=new Issue;
            $issue->name=$data['name'];
            $issue->status=$data['status'];
            $issue->assigned_to=$data['assigned_to'];
            $issue->os=$data['os'];
            $issue->url=$data['url'];
            $issue->authenticated=$data['authenticated'];
            $issue->error_msg=$data['error_msg'];
            $issue->expected_output=$data['expected_output'];
            $issue->notes=$data['notes'];
            if (Auth::guest()){
                $issue->updated_by='Undetermined';
                $issue->created_by='Undetermined';
            } else {
                $issue->updated_by=$this->getFullname(Auth::user()->username);
                $issue->created_by=$this->getFullname(Auth::user()->username);
            }

            $issue->save();
            return true;
        }

        public function modify($data,$id){
            $issue=Issue::find($id);
            $issue->status=$data['status'];
            $issue->assigned_to=$data['assigned_to'];
            $issue->os=$data['os'];
            $issue->url=$data['url'];
            $issue->authenticated=$data['authenticated'];
            $issue->error_msg=$data['error_msg'];
            $issue->expected_output=$data['expected_output'];
            $issue->notes=$data['notes'];
            $issue->updated_by=$this->getFullname(Auth::user()->username);
            $issue->save();
            return true;
        }

        public function getFullname($username){
            $user= User::whereUsername($username)->first();
            return $user->id;
        }

        private function getFullnamesById($issue){
            $issue['assigned_name'] = User::getFullname($issue->assigned_to);
            $issue['updated_name'] = User::getFullname($issue->updated_by);
            $issue['created_name'] = User::getFullname($issue->created_by);
        }



    }