<?php

use Repositories\StatusRepositoryInterface;

class StatusesController extends \BaseController {
    protected $status;

    public function __construct(StatusRepositoryInterface $status){
        $this->status=$status;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $statuses= Status::orderBy('status','ASC')->paginate(10);
        return View::make('statuses.index')
            ->with('title','All Statuses')
            ->with('statuses',$statuses);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $route=$this->status->createRoute();
        return View::make($route);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $errors = $this->status->isValid($input = Input::all());
        if (count( $errors)!=0){
            return Redirect::back()->withInput()->withErrors($errors);
        }

        $status=new Status;
        $status->status=Input::get('status');
        $status->save();

        return $this->index();

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($key)
	{
        $status=Status::where('status','=',$key)->first();

        return View::make('statuses.edit')
            ->with('status',$status);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($key)
	{
        $errors = $this->status->isValid($input = Input::all());

        if (count( $errors)!=0){
            return Redirect::back()->withInput()->withErrors($errors);
        }

        $status=Status::where('status','=',$key)->first();

        $status->status=Input::get('status');

        $status->save();

        return $this->index();
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($key)
	{
        $status=Status::where('status','=',$key)->first();
        $status->delete();

        Session::flash('message','Successfully deleted');
        return $this->index();

	}


}
