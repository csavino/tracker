<?php
/**
 * Created by PhpStorm.
 * User: winpress
 * Date: 4/12/14
 * Time: 8:01 PM
 */


class SessionsController extends BaseController {

    public function create () {

        return View::make('sessions.create');
    }

    public function store () {


        if(Auth::attempt(array('username'=>Input::get('username'),'password'=>Input::get('password'))))
        {
            return Redirect::intended();
        }

        return Redirect::to('login');

    }

    public function destroy() {

        Auth::logout();

        return Redirect::route('sessions.create');
    }

}