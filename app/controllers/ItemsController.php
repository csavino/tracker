<?php


use Repositories\ItemRepositoryInterface;

class ItemsController extends \BaseController {

    protected $item;

    public function __construct(ItemRepositoryInterface $item){
        $this->item=$item;
    }
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

        $query=Request::get('q');
        if (!empty($query)){

            $items= $this->item->indexQuery($query);
        } else {
            $query = 'x';   //when empty, resolves to a bad routein the View::make
            $items= $this->item->indexAll();
        }

        $route=$this->item->indexRoute();

        return View::make($route)
            ->with('title','All '.$this->item->focus)
            ->with('issues',$items)
            ;
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $user=new User;
        $user_options = $user->userOptions();
        $status=new Status;
        $status_options = $status->statusOptions();
        $route=$this->item->createRoute();
        return View::make($route)
            ->with('user_options',$user_options)
            ->with('status_options',$status_options);
  }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{

            //returns the array of error messages.
             $errors = $this->item->isValid($input = Input::all());
            $data = Input::all();

            if ($data['status']=='default'){
                if(!$errors){
                    $errors = new \Illuminate\Support\MessageBag();
                }
                $errors->add('status','The status field is required');
            }

            if ($data['assigned_to']=='default'){
                if(!$errors){
                    $errors = new \Illuminate\Support\MessageBag();
                }
                $errors->add('assigned_to','The assigned to field is required');
            }


               // return count($errors);
             if (count( $errors)!=0){
                return Redirect::back()->withInput()->withErrors($errors);
              }


        $this->item->add($data);

        return $this->index();
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
        $item=$this->item->find($id);

        $route=$this->item->showRoute();
        return View::make($route)
            ->with($this->item->focusItem,$item);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{

        if(Auth::guest()){
            return Redirect::to('login');
        }

        $item=$this->item->find($id);
        $user=new User;
        $user_options = $user->userOptions();
        $status=new Status;
        $status_options = $status->statusOptions();
        $route=$this->item->editRoute();
        return View::make($route)
            ->with($this->item->focusItem,$item)
            ->with('user_options',$user_options)
            ->with('status_options',$status_options);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $errors = $this->item->isValid($input = Input::all());

        /*
        if ($count !=0){
            return Redirect::back()->withInput()->withErrors($errors);
        }
        */

        $data = Input::all();
        $this->item->modify($data,$id);

        Session::flash('message','Successfully updated');

        return $this->show($id);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
        if(Auth::guest()){
            return Redirect::to('login');
        }

        $item=$this->item->find($id);
        $item->delete();

        Session::flash('message','Successfully deleted');
        return $this->index();
	}

}
