<?php

use Repositories\UserRepositoryInterface;



class UsersController extends \BaseController {

    protected $user;

    public function __construct(UserRepositoryInterface $user){
        $this->user=$user;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $users= User::orderBy('firstname','DSC')->paginate(10);
        return View::make('users.index')
            ->with('title','All Users')
            ->with('users',$users);
    }


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        $route=$this->user->createRoute();
        return View::make($route);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
       $errors = $this->user->isValid($input = Input::all());
       if (count( $errors)!=0){
            return Redirect::back()->withInput()->withErrors($errors);
        }

        $user=new User;
        $user->firstname=Input::get('firstname');
        $user->lastname=Input::get('lastname');
        $user->fullname = $user->firstname.' '.$user->lastname;
        $user->username=Input::get('username');
        $user->password=Input::get('password');  //Hashing done in User model with the setPasswordAttribute mutator
        $user->email=Input::get('email');
        $user->save();

        return $this->index();

	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user=User::find($id);

        return View::make('users.show')
            ->with('user',$user);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        $user=User::find($id);

        return View::make('users.edit')
            ->with('user',$user);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
        $errors = $this->user->isValid($input = Input::all());

        if (count( $errors)!=1){
            return Redirect::back()->withInput()->withErrors($errors);
        }

        $user = User::find($id);
        $user->firstname = Input::get('firstname');
        $user->lastname = Input::get('lastname');
        $user->fullname = $user->firstname.' '.$user->lastname;
        $user->username = Input::get('username');
        $user->email = Input::get('email');
        $user->save();

        Session::flash('message','Successfully updated');
        return View::make('users.show')
            ->with('user',$user);

	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{

        $user=$this->user->find($id);
        $user->delete();

        Session::flash('message','Successfully deleted');
        return $this->index();
	}


}
