<?php
/**
 * Created by PhpStorm.
 * User: winpress
 * Date: 4/11/14
 * Time: 10:26 AM
 */

    class Issue extends Eloquent{

        //relationships

        public function user(){
            return $this->belongsTo('User','assigned_to','id'); //assigned_to=local key, id = parent key
        }

        public static $rules = [
            'name'=>'required|unique:issues',
            'status'=>'required',
            'assigned_to'=>'required',
            'os'=>'required',
            'url'=>'required',
            'authenticated'=>'required',
            'error_msg'=>'required',
            'expected_output'=>'required',
        ];

        protected $fillable= ['name','status','assigned_to','os','url','authenticated','error_msg','expected_output','updated_by'];

        public $messages;

        public function isValid($data){

            $msgs=array(
              'name.required'=>'The title field is required',
            );

            $validation = Validator::make($data, static::$rules,$msgs);

            if($validation->passes()) return array();

            $this->messages = $validation->messages();

            return $this->messages;
        }
    }