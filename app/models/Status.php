<?php


class Status extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'statuses';

    public $messages;

    protected $fillable= ['status'];

    protected $primaryKey = 'status';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

    public $rules = [
        'status'=>'required|unique:statuses',

    ];

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getStatusIdentifier()
	{
		return $this->getKey();
	}

    public function isValid($data){

        $msgs=array();

        $validation = Validator::make($data, $this->rules,$msgs);

        if($validation->passes()) return array();

        $this->messages = $validation->messages();

        return $this->messages;
    }

    public function statusOptions() {
        // queries the statuses db table, orders by status and lists status
        $status_options = DB::table('statuses')->orderBy('status', 'asc')->lists('status','status');
        return $status_options;
    }
}