<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

    //Relationships

    public function issues(){
        return $this->hasMany('Issue','assigned_to','id');  //assigned_to=foreign key, id = local key.
    }

    public static $rules = [
        'firstname'=>'required',
        'lastname'=>'required',
        'username'=>'required',
        'password'=>'required',
        'email'=>'required',

    ];

    protected $fillable= ['firstname','lastname','username','email','password'];

    public $messages;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

    public function isValid($data){

         $msgs=array();

         $validation = Validator::make($data, static::$rules);

         if($validation->passes()) return array();

         $this->messages = $validation->messages();

         return $this->messages;
    }

    public static function getFullname($id){
        $user= static::find($id);
        if($user){
        return $user->firstname .' '.$user->lastname;
        } else {
            return 'Undetermined';
        }
    }

    public function userOptions() {
        // queries the users db table, orders by full name and lists fullname and id
        $user_options = DB::table('users')->orderBy('firstname', 'asc')->orderBy('lastname', 'asc')->lists('firstname','id');
        foreach($user_options as $key=>$value){
            $user_options[$key] = $this->getFullname($key);
        }

        return $user_options;
    }


    public function getFullNameAttribute(){
        return ucfirst($this->firstname).' '.ucfirst($this->lastname);
    }

    public function setPasswordAttribute($value){
        $this->attributes['password'] = Hash::make($value);
    }
}