<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIssuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('issues', function(Blueprint $table)
		{
            $table->increments('id');
            $table->string('name')->unique;
            $table->string('status');
            $table->integer('assigned_to');
            $table->string('os');
            $table->string('url');
            $table->boolean('authenticated');
            $table->string('error_msg');
            $table->string('expected_output',500);
            $table->string('notes',1000);
            $table->integer('created_by');
            $table->integer('updated_by');
            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('issues', function(Blueprint $table)
		{
            Schema::drop('issues');
		});
	}

}
